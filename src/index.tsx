import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import AppProviderWrapper from './AppContext';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
  <React.StrictMode>
    <AppProviderWrapper>
      <App />
    </AppProviderWrapper>
  </React.StrictMode>,
);
