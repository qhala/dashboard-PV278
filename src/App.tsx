import React from 'react';
import PrivateRoute from './components/PrivateRoute';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import LandingPage from './LandingPage';
import HomePage from './Dashboard';
import Profile from './Profile';
import DetailPage from './Detail';
import { ThemeProvider } from '@emotion/react';
import { Box, CssBaseline, createTheme } from '@mui/material';
import DefaultRoute from './components/DefaultRoute';

const defaultTheme = createTheme();

const App = () => {
  return (
    <div>
      <ThemeProvider theme={defaultTheme}>
        <Box sx={{ display: 'flex' }}>
          <CssBaseline />
          <BrowserRouter>
            <Routes>
              <Route path="/profile" element={<PrivateRoute component={Profile} />} />
              <Route path="/home-page" element={<PrivateRoute component={HomePage} />} />
              <Route path="/home-page/:uuid" element={<PrivateRoute component={DetailPage} />} />
              <Route index path="/" element={<DefaultRoute component={LandingPage} />} />
            </Routes>
          </BrowserRouter>
        </Box>
      </ThemeProvider>
    </div>
  );
};

export default App;
