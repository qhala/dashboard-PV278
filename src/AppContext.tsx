import React, { PropsWithChildren, useContext, useState, createContext, useEffect } from 'react';
import { AppState, Auth0ContextInterface, Auth0Provider, useAuth0 } from '@auth0/auth0-react';
import history from './utils/history';
import { getConfig } from './config';
import { ApiType, createInstance } from './utils/api';
import { AxiosResponse } from 'axios';

const onRedirectCallback = (appState?: AppState) => {
  history.push(appState && appState.targetUrl ? appState.targetUrl : window.location.pathname);
};

// Please see https://auth0.github.io/auth0-react/interfaces/Auth0ProviderOptions.html
// for a full list of the available properties on the provider
const config = getConfig();

const providerConfig = {
  domain: config.domain,
  clientId: config.clientId,
  onRedirectCallback,
  authorizationParams: {
    redirect_uri: window.location.origin,
    ...(config.audience ? { audience: config.audience } : null),
  },
};

type AppContextType = Auth0ContextInterface & {
  customDataApi?: ApiType;
  apiMuni?: ApiType;
  updateData?: UpdateData;
  getData?: GetData;
};

// eslint-disable-next-line no-unused-vars
export type UpdateData = (team: string) => <D, T>(customData: D & { user?: string }, id?: string) => Promise<T | undefined>;
// eslint-disable-next-line no-unused-vars
export type GetData = <D, T>(team: string, id?: string, user?: string) => Promise<AxiosResponse<D, T> | undefined>;
export const AppContext = createContext<AppContextType>({} as AppContextType);
export const useAppContext = () => useContext(AppContext);

const AppProvider: React.FC<PropsWithChildren> = ({ children }) => {
  const auth0 = useAuth0();
  const [{ apiMuni, customDataApi }, setAllApis] = useState<{ apiMuni?: ApiType; customDataApi?: ApiType }>({});

  useEffect(() => {
    if (auth0.user) {
      const { sun } = auth0.user;
      setAllApis({
        apiMuni: createInstance(sun),
        customDataApi: createInstance(sun),
      });
    }
  }, [auth0.user]);

  const updateData: UpdateData =
    (team) =>
    async ({ user, ...customData }, id) =>
      customDataApi &&
      customDataApi.apiInstance.post(`/rest/customdata?q=${id ? ` "id": ${id}` : ''}"}`, {
        data: customData,
        team,
        ...(user && { user }),
        ...(id && { _id: id }),
      });
  const getData: GetData = async (team: string, id?: string, user?: string) =>
    customDataApi?.apiInstance.get(
      `/rest/customdata?q={"team":"${team || 'ucitele'}${id ? `", "_id": "${id}` : ''}${user ? `", "user": "${user}` : ''}"}`,
    );

  return <AppContext.Provider value={{ ...auth0, apiMuni, customDataApi, updateData, getData }}>{children}</AppContext.Provider>;
};

const AppProviderWrapper: React.FC<React.PropsWithChildren> = ({ children }) => {
  return (
    <Auth0Provider {...providerConfig}>
      <AppProvider>{children}</AppProvider>
    </Auth0Provider>
  );
};

export default AppProviderWrapper;
