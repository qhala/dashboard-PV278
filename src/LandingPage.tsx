import { useAuth0 } from '@auth0/auth0-react';
import { Box, Button, Typography } from '@mui/material';
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const LandingPage = () => {
  const navigate = useNavigate();
  const { user } = useAuth0();
  useEffect(() => {
    if (user) {
      navigate('/home-page');
    }
  }, [user]);

  return user ? null : (
    <Box sx={{ width: '100%', maxWidth: 500 }}>
      <Typography variant="h2" gutterBottom>
        Landing page
      </Typography>
      <Button
        variant="outlined"
        onClick={() => {
          navigate('/home-page');
        }}
      >
        To home page
      </Button>
    </Box>
  );
};

export default LandingPage;
