import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useAuth0 } from '@auth0/auth0-react';

const PrivateRoute: React.FC<{ component: React.ElementType }> = ({ component: Component, ...rest }) => {
  const { isAuthenticated, loginWithRedirect } = useAuth0();
  const location = useLocation();

  useEffect(() => {
    const fn = async () => {
      if (!isAuthenticated) {
        await loginWithRedirect({
          appState: { targetUrl: location.pathname },
        });
      }
    };
    fn();
  }, [isAuthenticated, loginWithRedirect, location]);

  return isAuthenticated === true ? <Component {...rest} /> : null;
};

export default PrivateRoute;
