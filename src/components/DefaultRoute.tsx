import React from 'react';
import { useAuth0 } from '@auth0/auth0-react';

const DefaultRoute: React.FC<{ component: React.ElementType }> = ({ component: Component, ...rest }) => {
  const { isLoading } = useAuth0();

  return isLoading === true ? null : <Component {...rest} />;
};

export default DefaultRoute;
