import { useMemo, useEffect, useState, useCallback } from 'react';
import { useLocation } from 'react-router-dom';
import { useAppContext } from '../AppContext';
import { ApiType } from './api';
import { AxiosResponse } from 'axios';

export const useType = () => {
  const { search } = useLocation();
  const type = useMemo(() => {
    return new URLSearchParams(search).get('type');
  }, [search]);
  return type;
};

// eslint-disable-next-line no-unused-vars
type Getter = <D, T>(id?: string, user?: string) => Promise<AxiosResponse<D, T> | undefined>;

export const useCustomData = (
  team: string,
  // eslint-disable-next-line no-unused-vars
): [Getter, (<D, T>(customData: D & { user?: string }, id?: string) => Promise<T | undefined>) | undefined] => {
  const { updateData, getData } = useAppContext();
  const getter: Getter = async (id?: string, user?: string) => getData?.(team, id, user);
  return [getter, updateData?.(team)];
};

export type RestType = { resource: string; rest?: ApiType };

export const useRest = (type: 'companies' | 'council' | 'job-offers' | 'demography'): RestType => {
  const { apiMuni } = useAppContext();
  const [{ resource, rest }, setRestApi] = useState<{ resource?: string; rest?: ApiType }>({});
  const typeMapper = useCallback(
    (resourceType: 'companies' | 'council' | 'job-offers' | 'demography') => {
      const apiTypes = {
        companies: {
          resource: 'brno-firmy',
          rest: apiMuni,
        },
        council: {
          resource: 'hlasovani-rady',
          rest: apiMuni,
        },
        'job-offers': {
          resource: 'job-offers',
          rest: apiMuni,
        },
        demography: {
          resource: 'demography',
          rest: apiMuni,
        },
      };
      return apiTypes[resourceType] || apiTypes.companies;
    },
    [type],
  );

  useEffect(() => {
    setRestApi(typeMapper(type));
  }, [type]);

  return { resource: resource || 'brno-firmy', rest };
};
