import { Badge, Box, Chip, Container, IconButton, Paper, Toolbar, Typography } from '@mui/material';
import Grid from '@mui/material/Grid2';
import MenuIcon from '@mui/icons-material/Menu';
import React, { useEffect, useState } from 'react';
import AvatarCmp from './components/Avatar';
import NotificationsIcon from '@mui/icons-material/Notifications';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import { styled } from '@mui/material/styles';
import { useRest } from './utils/helpers';
import { useParams } from 'react-router-dom';

const drawerWidth: string = '100%';

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

type JobOfferType = {
  title: string;
  description: string;
  tags: string[];
  requirements: string[];
};

const DetailPage = () => {
  const [open, setOpen] = useState(true);
  const [activeCompany, setActiveCompany] = useState<{ industry: string } | null>(null);
  const [jobOffersList, setJobOffersList] = useState<JobOfferType[] | null>(null);
  const { resource, rest } = useRest('companies');
  const { resource: jobOffers, rest: jobOffersRest } = useRest('job-offers');
  const toggleDrawer = () => {
    setOpen(!open);
  };

  const { uuid } = useParams();

  useEffect(() => {
    if (uuid && resource && jobOffers && rest && jobOffersRest) {
      const fetchData = async () => {
        const company = await rest.get<unknown, { industry: string; name: string }>(`${resource}/${uuid}`);
        const availbleOffers = await jobOffersRest.get<unknown, JobOfferType[]>(
          `${jobOffers}?q={"$or": [{"field": "${company.industry}"}, {"field": "Other"}]}`,
        );
        setActiveCompany(company);
        setJobOffersList(availbleOffers.slice(0, Math.round(Math.random() * 10)));
      };

      fetchData();
    }
  }, [resource, jobOffers, rest, jobOffersRest, uuid]);

  return (
    <>
      <AppBar position="absolute" open={open}>
        <Toolbar
          sx={{
            pr: '24px', // keep right padding when drawer closed
          }}
        >
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawer}
            sx={{
              marginRight: '36px',
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
            Dashboard
          </Typography>
          <IconButton color="inherit">
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <AvatarCmp />
        </Toolbar>
      </AppBar>
      <Box
        component="main"
        sx={{
          backgroundColor: (theme) => (theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900]),
          flexGrow: 1,
          height: '100vh',
          overflow: 'auto',
        }}
      >
        <Toolbar />
        <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
          <Grid container spacing={3}>
            {jobOffersList &&
              jobOffersList.map((item, key) => (
                <Grid size={4} key={key}>
                  <Paper sx={{ height: '100%', width: '100%' }}>
                    <Typography>{item.title}</Typography>
                    <Typography>{item.description}</Typography>
                    {item.tags.map((tag, keyTag) => (
                      <Chip key={keyTag} label={tag} />
                    ))}
                    {item.requirements.map((tag, keyTag) => (
                      <Chip key={keyTag} label={tag} variant="outlined" />
                    ))}
                  </Paper>
                </Grid>
              ))}
            {/* Chart */}
            {/* <Grid size={8}>
              <Paper sx={{ height: '85%', width: '100%' }}></Paper>
            </Grid> */}
          </Grid>
        </Container>
      </Box>
    </>
  );
};

export default DetailPage;
