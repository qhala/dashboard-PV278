import React from 'react';
import { styled } from '@mui/material/styles';
import { useAuth0 } from '@auth0/auth0-react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid2';
import { Button, Paper, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';

const Profile: React.FC = () => {
  const { user } = useAuth0();

  const navigate = useNavigate();
  return (
    <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
      <Paper>
        <Grid container spacing={2}>
          <Grid size={12}>
            <Typography>Name: {user?.name}</Typography>
          </Grid>
          <Grid size={12}>
            <Typography>Username: {user?.nickname}</Typography>
          </Grid>
          <Grid size={12}>
            <Typography>
              <img alt="user image" src={user?.picture} />
            </Typography>
          </Grid>
        </Grid>
      </Paper>

      <Grid container spacing={2}>
        <Grid>
          <Button onClick={() => navigate('/home-page')} variant="outlined">
            To hompage
          </Button>
        </Grid>
      </Grid>
    </Container>
  );
};

const StyledProfile = styled(Profile)(({ theme }) => ({
  background: theme.palette.grey['400'],
}));

export default StyledProfile;
