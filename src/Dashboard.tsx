import * as React from 'react';
import { styled } from '@mui/material/styles';
import MuiDrawer from '@mui/material/Drawer';
import Box from '@mui/material/Box';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid2';
import Paper from '@mui/material/Paper';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import NotificationsIcon from '@mui/icons-material/Notifications';
import { useCustomData, useRest } from './utils/helpers';
import Avatar from './components/Avatar';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { Link } from 'react-router-dom';
import { useAppContext } from './AppContext';

const drawerWidth: number = 240;

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const queryBuilder = ({ skip, max }: { skip?: number; max?: number; filter?: string }) => `skip=${skip}&max=${max}`;

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(({ theme, open }) => ({
  '& .MuiDrawer-paper': {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxSizing: 'border-box',
    ...(!open && {
      overflowX: 'hidden',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9),
      },
    }),
  },
}));

const columns: GridColDef[] = [
  { field: 'name', headerName: 'Name', width: 150, renderCell: ({ row }) => <Link to={`/home-page/${row.id}`}>{row.name}</Link> },
  { field: 'website', headerName: 'Website', width: 150 },
  {
    field: 'industry',
    headerName: 'Industry',
    width: 150,
  },
  {
    field: 'turnover',
    headerName: 'Turnover',
    description: 'This column has a value getter and is not sortable.',
    width: 150,
  },
];

type RowType = {
  name?: string;
  website?: string;
  industry?: string;
  turnover?: string;
};

type ResponseType = {
  data: (RowType & { turnover_i: string; _id: string })[];
  totals: {
    total?: number;
    count?: number;
  };
};

export default function Dashboard() {
  const { user } = useAppContext();
  const [open, setOpen] = React.useState(true);
  const [pagination, setPagination] = React.useState({ skip: 0, max: 15 });
  const [total, setTotal] = React.useState(0);
  const [rows, setRows] = React.useState<RowType[]>([]);
  const toggleDrawer = () => {
    setOpen(!open);
  };

  console.log(user, 'this is user');

  const { resource, rest } = useRest('companies');
  const { resource: jobOffers, rest: jobOffersRest } = useRest('job-offers');
  console.log(jobOffersRest, jobOffers, 'this is job offers');
  const [getData, setData] = useCustomData('ucitele');

  React.useEffect(() => {
    const updater = async () => {
      // const prev = await getData?.('6756ae3c9466383100024f0c');
      setData?.(
        {
          // ...prev,
          bar: 'foo',
          user: user?.email,
        },
        '6756ae3c9466383100024f0c',
      );
      // get single record
      getData?.('6756ae3c9466383100024f0c');
      // get record for user with email
      getData?.(undefined, user?.email);
      getData?.();
    };
    updater();
  }, []);

  const fetchData = React.useCallback(
    async (skip?: number, max?: number) => {
      return (
        (await rest?.get<unknown, ResponseType>(
          `${resource}?${queryBuilder({
            skip,
            max,
          })}&totals=true`,
        )) ?? {
          data: [],
          totals: {},
        }
      );
    },
    [rest, resource],
  );

  React.useEffect(() => {
    // Let's log response from brno companies
    const query = async () => {
      const { data, totals } = await fetchData(pagination.skip, pagination.max);
      setRows(
        data.map((item) => ({
          id: item._id,
          turnover: item.turnover_i,
          ...item,
        })),
      );
      setTotal(totals.total || 0);
    };
    query();
  }, [rest, resource, pagination.max, pagination.skip]);

  return (
    <>
      <AppBar position="absolute" open={open}>
        <Toolbar
          sx={{
            pr: '24px', // keep right padding when drawer closed
          }}
        >
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawer}
            sx={{
              marginRight: '36px',
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
            Dashboard
          </Typography>
          <IconButton color="inherit">
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <Avatar />
        </Toolbar>
      </AppBar>
      <Drawer variant="permanent" open={open}>
        <Toolbar
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            px: [1],
          }}
        >
          <IconButton onClick={toggleDrawer}>
            <ChevronLeftIcon />
          </IconButton>
        </Toolbar>
        <Divider />
        <List component="nav">
          Here be dragons!
          <Divider sx={{ my: 1 }} />
          Here be another dragons!
        </List>
      </Drawer>
      <Box
        component="main"
        sx={{
          backgroundColor: (theme) => (theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900]),
          flexGrow: 1,
          height: '100vh',
          overflow: 'auto',
        }}
      >
        <Toolbar />
        <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
          <Grid container spacing={3}>
            {/* Chart */}
            <Grid size={8}>
              <Paper sx={{ height: 400, width: '100%' }}>
                <DataGrid
                  rows={rows}
                  columns={columns}
                  onPaginationModelChange={({ page, pageSize }) => {
                    setPagination({ skip: page * pageSize, max: pageSize });
                  }}
                  initialState={{
                    pagination: {
                      paginationModel: {
                        page: Math.floor(pagination.skip / pagination.max),
                        pageSize: pagination.max,
                      },
                    },
                  }}
                  pageSizeOptions={[15, 50, 100]}
                  sx={{ border: 0 }}
                  paginationMode="server"
                  rowCount={total}
                />
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  );
}
